package app;

import com.beust.jcommander.JCommander;
import app.mvc.view.SimulationView;
import app.mvc.controller.SimController;
import app.mvc.controller.SimulationController;
import app.mvc.controller.executor.BodySimulationService;
import app.mvc.model.SimModel;
import app.mvc.model.SimulationModel;
import com.beust.jcommander.Parameter;
import gov.nasa.jpf.vm.ThreadList;

import java.util.concurrent.CountDownLatch;

public class SimLauncher {

    @FunctionalInterface
    interface InRangeChecker {
        void check(int arg, int min, int max);
    }

    private static final int WIDTH = 620;
    private static final int HEIGHT = 620;
    private static final double BOUNDARY_WIDTH = 20;
    private static final double BOUNDARY_HEIGHT = 20;
    private static final int MIN_STEPS = 0;
    private static final int MAX_STEPS = 100_000;
    private static final int MIN_NBODIES = 1;
    private static final int MAX_NBODIES = 5_000;
    private static final int MIN_NTASKS = 1;
    private static final int MAX_NTASKS = 5_000;
    private static final int MIN_POOL_SIZE = 1;
    private static final int MAX_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    public static class Args {
        @Parameter(names = "-nSteps", description = "Steps of the simulation")
        private int nSteps;

        @Parameter(names = "-nBodies", description = "Number of bodies")
        private int nBodies;

        @Parameter(names = "-poolSize", description = "Number of threads to use")
        private int poolSize;

        @Parameter(names = "-nTasks", description = "Number of tasks to use")
        private int nTasks;

        @Parameter(names = "-guiEnabled", description = "GUI or command line app")
        private boolean guiEnabled = false;

        @Parameter(names = "-debug", description = "Debug mode")
        private boolean debugLogs = false;

        @Parameter(names = "--help", help = true)
        private boolean help = false;
    }

    public static void main(String... argv) throws InterruptedException {
        Args args = new Args();
        JCommander jct = JCommander.newBuilder()
                .addObject(args)
                .build();
        jct.parse(argv);
        if (args.help) {
            jct.usage();
            System.exit(0);
        }

        int nSteps = args.nSteps;
        int nBodies = args.nBodies;
        int nTasks = args.nTasks;
        int poolSize = args.poolSize;
        boolean debugLogs = args.debugLogs;
        boolean guiEnabled = args.guiEnabled;

        InRangeChecker pChecker = (value, min, max) -> {
            if (value < min || value > max) {
                System.err.println(value + " must be between " + min + " and " + max);
                System.err.println("Current is: " + value);
                System.exit(0);
            }
        };

        pChecker.check(nSteps, MIN_STEPS, MAX_STEPS);
        pChecker.check(nBodies, MIN_NBODIES, MAX_NBODIES);
        pChecker.check(nTasks, MIN_NTASKS, MAX_NTASKS);
        pChecker.check(poolSize, MIN_POOL_SIZE, MAX_POOL_SIZE);

        SimModel model = new SimulationModel(BOUNDARY_WIDTH, BOUNDARY_HEIGHT, nSteps, nBodies);
        SimulationView view = new SimulationView(WIDTH, HEIGHT);

        CountDownLatch latch = new CountDownLatch(1);
        BodySimulationService service = new BodySimulationService(model, poolSize, nTasks, debugLogs, latch);
        SimController controller = new SimulationController(service, latch);

        service.start();

        if (guiEnabled) {
            service.addListener(view);
            view.createGUI();
            view.display();
            view.addListener(controller);
        } else {
            Thread.sleep(1000);
            controller.start();
            service.join();
            System.exit(0);
        }
    }
}
