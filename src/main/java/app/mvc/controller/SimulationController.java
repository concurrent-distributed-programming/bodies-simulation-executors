package app.mvc.controller;

import app.mvc.controller.executor.BodySimulationService;

import java.util.concurrent.CountDownLatch;

public class SimulationController implements SimController {

	private final BodySimulationService service;
	private final CountDownLatch latch;

	public SimulationController(BodySimulationService service, CountDownLatch latch){
		this.service = service;
		this.latch = latch;
	}

	@Override
	public void start() {
		latch.countDown();
	}

	public void stopped() {
		service.requestShutdown();
	}

}
