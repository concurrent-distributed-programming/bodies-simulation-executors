package app.mvc.controller.executor;

import app.util.Body;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class Task implements Callable<List<Body>> {

	private final List<Body> bodies;
	private final ComputationStrategy strategy;

	public Task(List<Body> bodies, ComputationStrategy strategy) {
		this.bodies = bodies;
		this.strategy = strategy;
	}

	public List<Body> call() {
		return bodies.stream().map(strategy::compute).collect(Collectors.toList());
	}
}
