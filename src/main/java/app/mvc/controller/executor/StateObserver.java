package app.mvc.controller.executor;

import app.mvc.view.SimView;
import app.util.Body;

import java.util.List;

public interface StateObserver {
    void addListener(SimView view);
    void notifyUpdate();
    void notifyStateChange(String stateDescription);
}
