package app.mvc.controller.executor;

import app.util.Body;
import java.util.List;

@FunctionalInterface
public interface TaskAssignmentStrategy {
    void distribute(ComputationStrategy strategy);
}
