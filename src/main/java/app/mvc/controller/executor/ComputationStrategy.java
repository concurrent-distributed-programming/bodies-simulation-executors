package app.mvc.controller.executor;

import app.util.Body;

@FunctionalInterface
public interface ComputationStrategy {
    Body compute(Body b);
}
