package app.mvc.controller.executor;

import app.mvc.model.SimModel;
import app.mvc.view.SimView;
import app.util.Body;

import java.util.*;
import java.util.concurrent.*;

public class BodySimulationService extends Thread implements StateObserver {

	private static final int MS_BETWEEN_FRAMES = 1;
	private final SimModel model;
	private final int nTasks;
	private boolean shutdownRequested = false;
	private final boolean debugLogsEnabled;
	private final ArrayList<SimView> listeners;
	private final ExecutorService executor;
	private final CountDownLatch startupLatch;
	private final List<List<Body>> bodiesToTask;

	public BodySimulationService(SimModel model, int poolSize, int nTasks, boolean debugLogsEnabled, CountDownLatch latch) {
		this.model = model;
		this.nTasks = nTasks;
		this.startupLatch = latch;
		this.debugLogsEnabled = debugLogsEnabled;
		listeners = new ArrayList<>();
		executor = Executors.newFixedThreadPool(poolSize); // TODO: test newCacheThreadPool
		bodiesToTask = new ArrayList<>();
		createBodiesToTask();
	}

	private void createBodiesToTask() {
		/*
		 * Task distribution.
		 */
		int nBodies = model.getNBodies();
		List<Body> bodies = model.getBodies();
		int chunkSize = nBodies / nTasks;

		for(int i = 0; i < nTasks - 1; i++) {
			bodiesToTask.add(bodies.subList(i * chunkSize, (i + 1) * chunkSize));
			log(String.valueOf(bodiesToTask.get(i).size()));
		}
		bodiesToTask.add(bodies.subList((nTasks - 1) * chunkSize, bodies.size()));
		log(String.valueOf(bodiesToTask.get(nTasks - 1).size()));
	}

	private void map(ComputationStrategy strategy, List<Future<List<Body>>> results) {
		for(int i = 0; i < nTasks; i++) {
			try {
				Future<List<Body>> f = executor.submit(new Task(bodiesToTask.get(i), strategy));
				results.add(f);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void reduce(List<Future<List<Body>>> results) {
		List<Body> bodies = new ArrayList<>();
		for (Future<List<Body>> res: results) {
			try {
				bodies.addAll(res.get());
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		results.clear();
		model.setBodies(bodies);
		bodiesToTask.clear();
		createBodiesToTask();
	}

	public void run() {
		try {
			startupLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<Future<List<Body>>> results = new ArrayList<>();
		long tStart = System.currentTimeMillis();
   		boolean maxStepsReached = false;

		while (!maxStepsReached && !shutdownRequested) {
			try {
				long tIterStart = System.currentTimeMillis();
				notifyStateChange("Processing...");

				map(model::updateBodyPosition, results);
				reduce(results);
				map(model::updateBodyVelocity, results);
				reduce(results);

				/* update the model */
				maxStepsReached = model.advanceVirtualTime();
				/* update the view */
				if (!shutdownRequested) {
					notifyUpdate();
					long tIterEnd = System.currentTimeMillis();
					long tIterElapsed = tIterEnd - tIterStart;
					log("iter: " + model.getNIter() + " time elapsed: " + tIterElapsed + " ms");
					waitForNextFrame(tIterStart);
				} else {
					log("interrupted");
					executor.shutdown();
					executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
				}
			} catch(Exception ex) {
				ex.printStackTrace();
				log(ex.getMessage());
			}
		}

		long tEnd = System.currentTimeMillis();
		long tElapsed = tEnd - tStart;
		System.out.println(" time: " + tElapsed);
		if (!shutdownRequested) {
			notifyStateChange("Completed");
		} else {
			notifyStateChange("Interrupted");
		}
	}

	private void log(String msg){
		if (debugLogsEnabled) {
			synchronized(System.out){
				System.out.println("[ Service ] " + msg);
			}
		}
	}

	private void waitForNextFrame(final long current) {
		final long dt = System.currentTimeMillis() - current;
		if (dt < MS_BETWEEN_FRAMES) {
			try {
				Thread.sleep(MS_BETWEEN_FRAMES - dt);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void requestShutdown() {
		if (!shutdownRequested) {
			shutdownRequested = true;
		}
	}

	@Override
	public void addListener(SimView view) {
		listeners.add(view);
	}

	@Override
	public void notifyUpdate() {
		if(!listeners.isEmpty()) {
			List<Body> dCopy = new ArrayList<>();
			for(Body b: model.getBodies()) {
				dCopy.add(new Body(b));
			}
			for (SimView v: listeners){
				v.update(dCopy,
						model.getVirtualTime(),
						model.getNIter(),
						model.getBoundary()
				);
			}
		}
	}

	@Override
	public void notifyStateChange(String stateDescription) {
		for (SimView v: listeners){
			v.changeState(stateDescription);
		}
	}
}
