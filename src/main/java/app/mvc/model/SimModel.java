package app.mvc.model;

import app.util.Body;
import app.util.Boundary;
import app.util.V2d;

import java.util.ArrayList;
import java.util.List;

public interface SimModel {
    Body updateBodyVelocity(Body b);
    Body updateBodyPosition(Body b);
    V2d computeTotalForceOnBody(Body b);
    int getNBodies();
    boolean advanceVirtualTime();
    double getVirtualTime();
    Boundary getBoundary();
    List<Body> getBodies();
    void setBodies(List<Body> bodies);
    long getNIter();
}
